FROM zabbix/zabbix-server-pgsql:ubuntu-5.4.7

USER root
RUN apt-get update && apt-get install vim curl perl -y
